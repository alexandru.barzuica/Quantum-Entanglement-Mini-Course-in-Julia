{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lecture 4: Entanglement Measures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 4.1 Entropy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"display: flex;\">\n",
    "    <span style=\"white-space: nowrap;\">Entropy =</span>\n",
    "    <span style=\"margin-left: 10px;\">special measure that tells us how connected two parts of a quantum system are. Imagine we have a big toy made up of two smaller toys that are stuck together. When we look at just one of the small toys, we can get an idea of its state by using something called a reduced density matrix. If the entropy is not zero, it means the small toy is not in a simple state, it's mixed up. This tells us that the two small toys are connected in a way called entanglement. So the entanglement entropy helps us understand how tangled and connected things are in the quantum world!</span>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A Bipartite system consists of 2 distinct parts, in our case we are going to use:\n",
    "\n",
    "**psi = |0⟩ ⊗ |0⟩ + |1⟩ ⊗ |1⟩, where |0⟩ and |1⟩ are basis states of the subsystems**\n",
    "\n",
    "In the expression \"|0⟩ ⊗ |0⟩,\" we have two subsystems, and each subsystem has a state called |0⟩. The ⊗ symbol means we combine the states of both subsystems. When we combine the states using the tensor product, we get a new set of possible states for the combined system. In this case, combining the |0⟩ state from both subsystems gives us the state |00⟩.\n",
    "\n",
    "Similarly, in the expression \"|1⟩ ⊗ |1⟩,\" we again have two subsystems, and this time each subsystem is in the state |1⟩. When we combine these states using the tensor product, we get another new state for the combined system. Combining the |1⟩ state from both subsystems gives us the state |11⟩.\n",
    "\n",
    "So, overall, we have a composite system made up of two subsystems. In one case, both subsystems are in the state |0⟩, giving us the state |00⟩. In the other case, both subsystems are in the state |1⟩, giving us the state |11⟩. This means the composite system can exist in different combinations of these two states, and the subsystems are connected in a way that their states are related to each other."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Density Matrix:ComplexF64[0.4999999999999999 + 0.0im 0.0 + 0.0im 0.0 + 0.0im 0.4999999999999999 + 0.0im; 0.0 + 0.0im 0.0 + 0.0im 0.0 + 0.0im 0.0 + 0.0im; 0.0 + 0.0im 0.0 + 0.0im 0.0 + 0.0im 0.0 + 0.0im; 0.4999999999999999 + 0.0im 0.0 + 0.0im 0.0 + 0.0im 0.4999999999999999 + 0.0im]\n"
     ]
    }
   ],
   "source": [
    "using LinearAlgebra\n",
    "\n",
    "function generate_density_matrix(psi)\n",
    "    dim = length(psi)\n",
    "    rho = zeros(ComplexF64, dim, dim)\n",
    "    \n",
    "    for i in 1:dim\n",
    "        for j in 1:dim\n",
    "            rho[i, j] = psi[i] * conj(psi[j])\n",
    "        end\n",
    "    end\n",
    "    \n",
    "    return rho\n",
    "end\n",
    "\n",
    "psi = [1/sqrt(2), 0, 0, 1/sqrt(2)]\n",
    "rho = generate_density_matrix(psi)\n",
    "\n",
    "println(\"Density Matrix:\", rho)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Entanglement entropy: 3.2034265038149176e-16\n"
     ]
    }
   ],
   "source": [
    "using LinearAlgebra\n",
    "\n",
    "function compute_entanglement_entropy(rho)\n",
    "    eigenvals = eigen(rho).values\n",
    "    nonzero_eigenvals = eigenvals[eigenvals .> 1e-12]\n",
    "    entropy = -sum(nonzero_eigenvals .* log2.(nonzero_eigenvals))\n",
    "    return entropy\n",
    "end\n",
    "\n",
    "entanglement_entropy = compute_entanglement_entropy(rho)\n",
    "\n",
    "println(\"Entanglement entropy: \", entanglement_entropy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "<span style=\"color:green; font-weight:bold;\">The value above hopefully should be a very small number close to zero. It indicates that the two subsystems in the composite system are highly entangled, meaning their states are intricately connected and cannot be described independently. The closer the entanglement entropy value is to zero, the stronger the entanglement between the subsystems.</span>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below we will try a similar thing, but this time we are going to use random values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Entropy: 4.330437500103084\n"
     ]
    }
   ],
   "source": [
    "using LinearAlgebra\n",
    "\n",
    "function entropy(tensor::AbstractArray{T, N}) where {T, N}\n",
    "    tensor = normalize(tensor)\n",
    "    entropy_val = -sum(tensor .* log2.(tensor))\n",
    "    \n",
    "    return entropy_val\n",
    "end\n",
    "\n",
    "function normalize(tensor::AbstractArray{T, N}) where {T, N}\n",
    "    total_sum = sum(tensor)\n",
    "    tensor = tensor ./ total_sum\n",
    "    return tensor\n",
    "end\n",
    "\n",
    "prob_tensor = rand(Float64, 3, 3, 3)\n",
    "entropy_val = entropy(prob_tensor)\n",
    "\n",
    "println(\"Entropy: \", entropy_val)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "<span style=\"color:green; font-weight:bold;\">Hopefully the Entropy value from above is **A LOT** bigger than in our first example, because we chose values at random that should lead to a system which entanglement is very low..</span>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 4.2 Concurrence"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"display: flex;\">\n",
    "    <span style=\"white-space: nowrap;\">Concurrence =</span>\n",
    "    <span style=\"margin-left: 10px;\">measure of entanglement that quantifies the degree of entanglement between two qubits in a quantum system. It's the opposite of entropy, concurrence value ranges from 0 to 1, where 0 indicates no entanglement and 1 represents maximum entanglement.</span>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is the algorithmic method for calculating concurrence: \n",
    "\n",
    "- Compute the matrix R, which is derived from the density matrix: R = ρ * (σ_y ⊗ σ_y) * ρ^* * (σ_y ⊗ σ_y) Here, ρ^* denotes the complex conjugate of ρ, and σ_y is the Pauli-Y matrix.\n",
    "\n",
    "- Compute the eigenvalues (λ_i) of the matrix R.\n",
    "\n",
    "- Sort the eigenvalues in decreasing order.\n",
    "\n",
    "- Calculate the concurrence using the formula: C = max(0, √(λ_1) - √(λ_2) - √(λ_3) - √(λ_4)) Here, λ_1, λ_2, λ_3, and λ_4 are the eigenvalues of R.\n",
    "\n",
    "Pauli matrices re a set of three 2x2 matrices widely used in quantum mechanics. They play a fundamental role in describing and manipulating quantum systems."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we did before we are going to run this 2 times:\n",
    "\n",
    "- first on our favorite Bell State **psi = |0⟩ ⊗ |0⟩ + |1⟩ ⊗ |1⟩, where |0⟩ and |1⟩ are basis states of the subsystems**\n",
    "- second on a randomly generated vector"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Concurrence: 0.9999999764391955\n"
     ]
    }
   ],
   "source": [
    "using LinearAlgebra\n",
    "\n",
    "psi = kron([1, 0], [1, 0]) + kron([0, 1], [0, 1])\n",
    "ρ = psi * psi'\n",
    "ρ /= sum(diag(ρ))\n",
    "σ_y = [0 -im;\n",
    "       im 0]\n",
    "R = ρ * (kron(σ_y, σ_y)) * ρ' * (kron(σ_y, σ_y))\n",
    "λ = eigen(R).values\n",
    "λ_sorted = sort(abs.(λ), rev=true)\n",
    "\n",
    "C = max(0, √(λ_sorted[1]) - √(λ_sorted[2]) - √(λ_sorted[3]) - √(λ_sorted[4]))\n",
    "println(\"Concurrence: \", C)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style=\"color:green; font-weight:bold;\">Hopefully the Concurrence value from above is very close to 1.</span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Concurrence: 0.0\n"
     ]
    }
   ],
   "source": [
    "using LinearAlgebra\n",
    "using Random\n",
    "\n",
    "Random.seed!(123)\n",
    "\n",
    "ψ = rand(ComplexF64, 4)\n",
    "ψ /= norm(ψ)\n",
    "\n",
    "ρ = ψ * ψ'\n",
    "\n",
    "σ_y = [0 -im;\n",
    "       im 0]\n",
    "R = ρ * (kron(σ_y, σ_y)) * ρ' * (kron(σ_y, σ_y))\n",
    "\n",
    "λ = eigen(R).values\n",
    "λ_abs = abs.(λ)\n",
    "\n",
    "C = max(0, √(λ_abs[1]) - √(λ_abs[2]) - √(λ_abs[3]) - √(λ_abs[4]))\n",
    "println(\"Concurrence: \", C)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style=\"color:green; font-weight:bold;\">Hopefully the Concurrence value from above is very close to 1.</span>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.8.5",
   "language": "julia",
   "name": "julia-1.8"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
