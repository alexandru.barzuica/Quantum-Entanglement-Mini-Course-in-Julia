# Quantum Entanglement Mini Course In Julia



## Download Julia 1.8.5
In order to run the files you need Julia 1.8.5, download link:
https://julialang.org/downloads/

## Install additional packages
Once you finished installing it you need to:
- open the Julia terminal
- install several packages
-- using Pkg
-- Pkg.add(["Plots","Yao","IJulia"])
- you can install all of them at once but I recommand it installing 1 by 1
-- Pkg.add("Plots")
-- Pkg.add("Yao")
-- Pkg.add("IJulia")

## Navigate to where the files are stored on your device
- navigate to the directory with the files in them using the cd command
-- jupyter −notebook "Lecture_1.ipynb "
- that will launch Lecture_1 in your default browser
